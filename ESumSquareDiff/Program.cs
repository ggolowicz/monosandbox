using System;

namespace ESumSquareDiff
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			long s1 = 0;
			long s2 = 0;

			for (int i = 1; i<= 100; i++) {
				s1 += i*i;
				s2 += i;
			}

			long s3 = s2 * s2;
			Console.WriteLine(String.Format("{0} - {1} = {2}", s3, s1, s3-s1));
		}
	}
}
