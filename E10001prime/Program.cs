using System;

namespace E10001prime
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int primeIdx = 0;
			long i = 3;
			while (10002> primeIdx) {
				if (EulerLibrary.Euler.isPrimeNumber (i)) {
					primeIdx++;
					Console.WriteLine (String.Format ("{0} prime number is {1}", primeIdx, i));
				}
				i++;
			}

		}
	}
}
