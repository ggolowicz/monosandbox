using System;
using EulerLibrary;

namespace Console1
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			long number = 600851475143;
			long largestPrime = 0;

			long halfNum = (long) Math.Round((double) (number/2));

			for (long i = 1; i <= halfNum; i++) {
				if (0 == number%i && Euler.isPrimeNumber(i)) {
					largestPrime = i;
					Console.WriteLine (String.Format ("Current prime factor: {0}", i));
				}
			}


			Console.WriteLine (String.Format("Largest prime factor is: {0}", largestPrime));
		}

	}
}
