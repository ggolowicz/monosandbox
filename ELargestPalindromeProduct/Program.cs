using System;
using EulerLibrary;

namespace ELargestPalindromeProduct
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine (String.Format ("Largest palindrome is {0}", GetLargestPalindrome()));

		}

		private static long GetLargestPalindrome()
		{
			int largestPalindrome = 0;

			for (int x = 999; x > 0; x--) {
				for (int y = 999; y > 0; y--) {
					if (Euler.isPalindromeNumber(x * y)) {
						if (x * y > largestPalindrome) {
							largestPalindrome = x * y;
							Console.WriteLine (String.Format ("{0} * {1} = {2}", x, y, x * y));
						}

					}
				}
			}

			return largestPalindrome;
		}
	}
}
