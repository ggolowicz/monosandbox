using System;

namespace EulerLibrary
{
	public class Euler
	{
		public static bool isPrimeNumber(long number)
		{
			long halfNum = (long) Math.Round((double) (number/2));

			for (long i = 3; i <= halfNum; i++) {
				if (0 == number%i) {
					return false;
				}
			}

			return true;
		}

		public static bool isPalindromeNumber(long number)
		{
			return number.ToString().Equals(Reverse(number.ToString()));
		}

		public static string Reverse( string s )
		{
			char[] charArray = s.ToCharArray();
			Array.Reverse( charArray );
			return new string( charArray );
		}
	}
}

