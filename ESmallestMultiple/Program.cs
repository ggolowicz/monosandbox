using System;

namespace ESmallestMultiple
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			long multiple = 20;
			long number = multiple*2;
			while (!isMultiple(number, multiple)) {
				number = number + multiple;
				//Console.WriteLine(String.Format("Current try: {0}", number));
			}

			Console.WriteLine(String.Format("Smallest multiple is {0}", number));

		}

		public static bool isMultiple(long number, long multiple)
		{
			for (int i = 1; i <= multiple; i++) {
				if (0 != (number % i)) {
					return false;
				}
			}

			return true;
		}

	}
}
